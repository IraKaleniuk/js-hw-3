"use strict";

let number = prompt("Enter number:");

while (!Number.isInteger(+number) || number === null || number === "") {
    console.log("Sorry, no numbers");
    number = prompt("Enter number:");
}

if (number <= 0) {
    console.log("Sorry, no numbers");
} else {
    for (let i = 0; i < number; i++) {
        (i % 5 === 0) ? console.log(i) : "";
    }
}

console.log("Next task");
let numberStart = prompt("Enter natural number (start):");
let numberEnd = prompt("Enter natural number (end):");

while (!Number.isInteger(+numberStart) || numberStart === null || numberStart === "" || numberStart <= 1 ||
!Number.isInteger(+numberEnd) || numberEnd === null || numberEnd === "" || numberEnd <= 1 || numberEnd < numberStart) {
    numberStart = prompt("Enter natural number (start):");
    numberEnd = prompt("Enter natural number (end):");
}

console.log(`Prime numbers in [${numberStart}; ${numberEnd}] are:`)
for (let i = numberStart; i < numberEnd; i++) {
    let isPrime = true;
    for (let j = 2; j <= Math.sqrt(i); j++) {
        if (i % j === 0) {
            isPrime = false;
            break;
        }
    }
    isPrime ? console.log(i) : "";
}